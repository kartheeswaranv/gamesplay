package com.karthee.games.uimodule;

import android.content.Context;
import android.graphics.Typeface;
import com.karthee.games.uimodule.logger.LogUtils;

public class FontUtils {
  private static String TAG = LogUtils.Companion.makeLogTag(FontUtils.class.getSimpleName());

  private static final String ERASER_REGULAR = "eraser_regular.ttf";
  private static final String ERASER_BOLD = "eraser_dust.ttf";

  private static Typeface EraserRegularFont;
  private static Typeface EraserBoldFont;

  public static Typeface getUbuntuBoldFont(Context context) {
    if (EraserBoldFont == null) {
      EraserBoldFont = Typeface.createFromAsset(context.getAssets(), ERASER_BOLD);
    }
    return EraserBoldFont;
  }


  public static Typeface getUbuntuRegularFont(Context context) {
    if (EraserRegularFont == null) {
      EraserRegularFont = Typeface.createFromAsset(context.getAssets(), ERASER_REGULAR);
    }
    return EraserRegularFont;
  }

  public static Typeface getFontByName(Context context, String fontTTFName) {
    if (fontTTFName == null || context == null) {
      return null;
    }
    switch (fontTTFName) {
      case ERASER_REGULAR:
        return getUbuntuRegularFont(context);
      case ERASER_BOLD:
        return getUbuntuBoldFont(context);
    }

    return null;
  }

  public static Typeface getTypeface(Context context, String name) {

    return Typeface.createFromAsset(context.getAssets(), name);
  }

}
