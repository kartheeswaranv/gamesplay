package com.karthee.games.uimodule;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Paint;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.TextView;

public class TextViewCustom extends TextView {
  private static final String TAG = TextViewCustom.class.getSimpleName();

  private String type = "";
  private boolean mEnableStrikeTxt;

  public TextViewCustom(Context context, AttributeSet attrs, int defStyle) {

    super(context, attrs, defStyle);
    initView(context, attrs, defStyle);
  }

  public TextViewCustom(Context context, AttributeSet attrs) {
    super(context, attrs);
    initView(context, attrs, 0);
  }

  public TextViewCustom(Context context) {
    super(context);
  }

  public void initView(final Context context, AttributeSet attrs, int defStyle) {
    if (attrs != null) {
      TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.TextViewCustom, defStyle, 0);
      type = ta.getString(R.styleable.TextViewCustom_ttfName);
      mEnableStrikeTxt = ta.getBoolean(R.styleable.TextViewCustom_strikeOut, false);
      ta.recycle();
    }

    if (type == null) {
      type = getContext().getString(R.string.eraser_regular);
    }
    setTypeface(FontUtils.getFontByName(context, type));
    setStrikeOutText(mEnableStrikeTxt);
  }

  public void setTtfName(String name) {
    if (!TextUtils.isEmpty(name)) {
      type = name;
      setTypeface(FontUtils.getFontByName(getContext(), type));
    }
  }

  public void setStrikeOutText(boolean value) {
    if (value) {
      this.setPaintFlags(this.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
    }
  }
}

