package com.karthee.games.uimodule.logger

import android.util.Log
import android.view.View
import com.karthee.games.uimodule.BuildConfig

class LogUtils {


  companion object {
    private val LOG_PREFIX = "kar_"
    private val LOG_PREFIX_LENGTH = LOG_PREFIX.length
    private val MAX_LOG_TAG_LENGTH = 23

    fun makeLogTag(str: String?): String {
      return if (str?.length!! > MAX_LOG_TAG_LENGTH - LOG_PREFIX_LENGTH) {
        LOG_PREFIX + str?.substring(0, MAX_LOG_TAG_LENGTH - LOG_PREFIX_LENGTH - 1)
      } else LOG_PREFIX + str

    }

    fun logMeasureSpec(
      tag: String,
      widthMeasureSpec: Int,
      heightMeasureSpec: Int
    ) {

      val widthSize = View.MeasureSpec.getSize(widthMeasureSpec)
      val widthMode: String

      when (View.MeasureSpec.getMode(widthMeasureSpec)) {
        View.MeasureSpec.EXACTLY -> widthMode = "EXACTLY"
        View.MeasureSpec.AT_MOST -> widthMode = "AT_MOST"
        else -> widthMode = "UNSPECIFIED"
      }

      val heightSize = View.MeasureSpec.getSize(heightMeasureSpec)
      val heightMode: String

      when (View.MeasureSpec.getMode(heightMeasureSpec)) {
        View.MeasureSpec.EXACTLY -> heightMode = "EXACTLY"
        View.MeasureSpec.AT_MOST -> heightMode = "AT_MOST"
        else -> heightMode = "UNSPECIFIED"
      }

      LOGD(tag, "Width: ($widthMode, $widthSize)  Height: ($heightMode, $heightSize)")
    }

    fun logViewMeasuredDimensions(
      TAG: String,
      view: View?
    ) {
      if (view == null) {
        LOGD(TAG, "View is null. Cannot log dimensions")
      }

      LOGD(
          TAG, KeyValueLogBuilder().appendInt("Width", view!!.measuredWidth)
          .appendInt("Height", view.measuredHeight)
          .build()
      )
    }

    /**
     * Don't use this when obfuscating class names!
     */
    fun makeLogTag(cls: Class<*>): String {
      return makeLogTag(cls.simpleName)
    }

    fun LOGD(
      tag: String,
      message: String
    ) {
      //noinspection PointlessBooleanExpression,ConstantConditions
      if (BuildConfig.DEBUG) {
        Log.d(resolveTag(tag), message)
      }
    }

    fun LOGD(
      tag: String,
      message: String,
      cause: Throwable
    ) {
      //noinspection PointlessBooleanExpression,ConstantConditions
      if (BuildConfig.DEBUG) {
        Log.d(resolveTag(tag), message, cause)
      }
    }

    fun LOGV(
      tag: String,
      message: String
    ) {
      //noinspection PointlessBooleanExpression,ConstantConditions
      if (BuildConfig.DEBUG) {
        Log.v(resolveTag(tag), message)
      }
    }

    fun LOGV(
      tag: String,
      message: String,
      cause: Throwable
    ) {
      //noinspection PointlessBooleanExpression,ConstantConditions
      if (BuildConfig.DEBUG) {
        Log.v(resolveTag(tag), message, cause)
      }
    }

    private fun resolveTag(tag: String): String {
      return if (tag.length > 23) tag.substring(0, 23) else tag

    }

    fun LOGI(
      tag: String,
      message: String
    ) {
      Log.i(resolveTag(tag), message)
    }

    fun LOGI(
      tag: String,
      message: String,
      cause: Throwable
    ) {
      Log.i(resolveTag(tag), message, cause)
    }

    fun LOGW(
      tag: String,
      message: String
    ) {
      Log.w(resolveTag(tag), message)
    }

    fun LOGW(
      tag: String,
      message: String,
      cause: Throwable
    ) {
      Log.w(resolveTag(tag), message, cause)
    }

    fun LOGE(
      tag: String,
      message: String
    ) {
      Log.e(resolveTag(tag), message)
    }

    fun LOGE(
      tag: String,
      message: String,
      cause: Throwable
    ) {
      Log.e(resolveTag(tag), message, cause)
    }

    /**
     * Static factory for [KeyValueLogBuilder]
     *
     * @return new instance of [KeyValueLogBuilder]
     */
    fun newKeyValueLogBuilder(): KeyValueLogBuilder {
      return KeyValueLogBuilder()
    }

    /**
     * @author Gaurav Dingolia
     *
     * This is a helper class for chaining the key-value pair logs.
     */
    class KeyValueLogBuilder {

      private val messageBuilder = StringBuilder()

      private val builder: StringBuilder
        get() = if (messageBuilder.length > 0) messageBuilder.append(
            ENTITY_SEPARATOR
        ) else messageBuilder

      fun appendBoolean(
        key: String,
        value: Boolean
      ): KeyValueLogBuilder {
        builder.append(key)
            .append(KEY_VALUE_SEPARATOR)
            .append(value)
        return this
      }

      fun appendInt(
        key: String,
        value: Int
      ): KeyValueLogBuilder {
        builder.append(key)
            .append(KEY_VALUE_SEPARATOR)
            .append(value)
        return this
      }

      fun appendLong(
        key: String,
        value: Long
      ): KeyValueLogBuilder {
        builder.append(key)
            .append(KEY_VALUE_SEPARATOR)
            .append(value)
        return this
      }

      fun appendFloat(
        key: String,
        value: Float
      ): KeyValueLogBuilder {
        builder.append(key)
            .append(KEY_VALUE_SEPARATOR)
            .append(value)
        return this
      }

      fun appendDouble(
        key: String,
        value: Double
      ): KeyValueLogBuilder {
        builder.append(key)
            .append(KEY_VALUE_SEPARATOR)
            .append(value)
        return this
      }

      fun append(
        key: String,
        value: String
      ): KeyValueLogBuilder {
        builder.append(key)
            .append(KEY_VALUE_SEPARATOR)
            .append(value)
        return this
      }

      fun build(): String {
        return messageBuilder.toString()
      }

      companion object {
        private val KEY_VALUE_SEPARATOR = ':'
        private val ENTITY_SEPARATOR = ", "
      }
    }
  }
}
