package com.karthee.games.uimodule.progressviews

import android.content.Context
import android.graphics.*
import android.text.TextPaint
import android.text.TextUtils
import android.util.AttributeSet
import android.view.View
import com.karthee.games.mathplay.utils.UnitUtils
import com.karthee.games.uimodule.FontUtils
import com.karthee.games.uimodule.R


class ColorPercentageMeter  constructor(context: Context, attrs: AttributeSet? = null) :
    View(context, attrs) {

    private val mProgressRectF = RectF()
    private val mProgressTextRect = Rect()

    private val mProgressPaint = Paint(Paint.ANTI_ALIAS_FLAG)
    private val mProgressRotatePaint = Paint(Paint.ANTI_ALIAS_FLAG)
    private val mProgressBackgroundPaint = Paint(Paint.ANTI_ALIAS_FLAG)

    private val mProgressTextPaint = TextPaint(Paint.ANTI_ALIAS_FLAG)

    private var mRadius: Float = 0.toFloat()
    private var mCenterX: Float = 0.toFloat()
    private var mCenterY: Float = 0.toFloat()

    var mProgressGap :Int = 0
        set(mProgressGap) {
            field = mProgressGap
            invalidate()
        }
    var progress: Int = 0
        set(progress) {
            field = progress
            invalidate()
        }
    var max = DEFAULT_MAX
        set(max) {
            field = max
            invalidate()
        }

    //Only work well in the Line Style, represents the line count of the rings included
    private var mLineCount: Int = 0
    //Only work well in the Line Style, Height of the line of the progress bar
    private var mLineWidth: Float = 0.toFloat()

    //Stroke width of the progress of the progress bar
    private var mProgressStrokeWidth: Float = 0.toFloat()

    private var mProgressBackgroundStrokeWidth: Float = 0.toFloat()

    //Text size of the progress of the progress bar
    private var mProgressTextSize: Float = 0.toFloat()

    //Start color of the progress of the progress bar
    private var mProgressStartColor: Int = 0
    //End color of the progress of the progress bar
    private var mProgressEndColor: Int = 0
    //Color of the progress value of the progress bar
    private var mProgressTextColor: Int = 0
    //Background color of the progress of the progress bar
    private var mProgressBackgroundColor: Int = 0

    //the rotate degree of the canvas, default is -90.
    private var mStartDegree: Int = 0

    // whether draw the background only outside the progress area or not
    private var mDrawBackgroundOutsideProgress: Boolean = false

    //Format the current progress value to the specified format
    private var mProgressFormatter: ColorPercentageMeter.ProgressFormatter? =
        ColorPercentageMeter.DefaultProgressFormatter()

    private var mRotateCount = 0

    private var mEndProgress = 100

    //The style of the progress color
    private var mStyle: Int = 0

    //The Shader of mProgressPaint
    private var mShader: Int = 0
    //The Stroke Cap of mProgressPaint and mProgressBackgroundPaint
    private var mCap: Paint.Cap? = null

    var startDegree: Int
        get() = mStartDegree
        set(startDegree) {
            this.mStartDegree = startDegree
            invalidate()
        }

    var isDrawBackgroundOutsideProgress: Boolean
        get() = mDrawBackgroundOutsideProgress
        set(drawBackgroundOutsideProgress) {
            this.mDrawBackgroundOutsideProgress = drawBackgroundOutsideProgress
            invalidate()
        }

    /*@Retention(RetentionPolicy.SOURCE)
    @IntDef(LINE, SOLID, SOLID_LINE)
    private annotation class Style

    @Retention(RetentionPolicy.SOURCE)
    @IntDef(LINEAR, RADIAL, SWEEP)
    private annotation class ShaderMode*/

    init {
        initFromAttributes(context, attrs)
        initPaint()
    }

    /**
     * Basic data initialization
     */
    private fun initFromAttributes(context: Context, attrs: AttributeSet?) {
        val a = context.obtainStyledAttributes(attrs, R.styleable.ColorPercentageMeter)

        mStyle = a.getInt(R.styleable.ColorPercentageMeter_p_style, LINE)
        mShader = a.getInt(R.styleable.ColorPercentageMeter_p_shader, LINEAR)

        mProgressTextSize = a.getDimensionPixelSize(
            R.styleable.ColorPercentageMeter_p_text_size,
            UnitUtils.dip2px(getContext(), DEFAULT_PROGRESS_TEXT_SIZE)
        ).toFloat()
        mProgressStrokeWidth = a.getDimensionPixelSize(
            R.styleable.ColorPercentageMeter_p_stroke_width,
            UnitUtils.dip2px(getContext(), DEFAULT_PROGRESS_STROKE_WIDTH)
        ).toFloat()
        mProgressBackgroundStrokeWidth = a.getDimensionPixelSize(
            R.styleable.ColorPercentageMeter_p_background_stroke_width,
            UnitUtils.dip2px(getContext(), DEFAULT_PROGRESS_BACKGROUND_STROKE_WIDTH)
        ).toFloat()

        mProgressStartColor =
                a.getColor(R.styleable.ColorPercentageMeter_p_progress_start_color, Color.parseColor(COLOR_FFF2A670))
        mProgressEndColor =
                a.getColor(R.styleable.ColorPercentageMeter_p_progress_end_color, Color.parseColor(COLOR_FFF2A670))
        mProgressTextColor = a.getColor(R.styleable.ColorPercentageMeter_p_text_color, Color.parseColor(COLOR_FFF2A670))
        mProgressBackgroundColor =
                a.getColor(R.styleable.ColorPercentageMeter_p_background_color, Color.parseColor(COLOR_FFD3D3D5))

        mStartDegree = a.getInt(R.styleable.ColorPercentageMeter_p_start_degree, DEFAULT_START_DEGREE)
        mDrawBackgroundOutsideProgress =
                a.getBoolean(R.styleable.ColorPercentageMeter_p_drawBackgroundOutsideProgress, false)

        a.recycle()
    }

    /**
     * Paint initialization
     */
    private fun initPaint() {
        mProgressTextPaint.textAlign = Paint.Align.CENTER
        mProgressTextPaint.textSize = mProgressTextSize
        mProgressTextPaint.typeface = (FontUtils.getFontByName(context,context.getString(R.string.eraser_regular)))

        mProgressPaint.style = if (mStyle == SOLID) Paint.Style.FILL else Paint.Style.STROKE
        mProgressPaint.strokeWidth = mProgressStrokeWidth
        mProgressPaint.color = mProgressStartColor
        mProgressPaint.isAntiAlias = true
        //mProgressPaint.setStrokeCap(mCap);

        mProgressRotatePaint.style = if (mStyle == SOLID) Paint.Style.FILL else Paint.Style.STROKE
        mProgressRotatePaint.strokeWidth = mProgressStrokeWidth
        mProgressRotatePaint.color = mProgressEndColor
        mProgressRotatePaint.isAntiAlias = true
        //mProgressRotatePaint.setStrokeCap(mCap);

        mProgressBackgroundPaint.style = if (mStyle == SOLID) Paint.Style.FILL else Paint.Style.STROKE
        mProgressBackgroundPaint.strokeWidth = mProgressBackgroundStrokeWidth
        mProgressBackgroundPaint.color = mProgressBackgroundColor
        //mProgressBackgroundPaint.setStrokeCap(mCap);
        mProgressBackgroundPaint.isAntiAlias = true
    }

    fun setEndProgress(progress: Int) {
        mEndProgress = progress
        updateProgressShader()
    }

    /**
     * The progress bar color gradient,
     * need to be invoked in the [.onSizeChanged]
     */
    private fun updateProgressShader() {
        if (mProgressStartColor != mProgressEndColor) {
            var shader: Shader? = null
            when (mShader) {
                LINEAR -> {
                    shader = LinearGradient(
                        mProgressRectF.left, mProgressRectF.top,
                        mProgressRectF.left, mProgressRectF.bottom,
                        mProgressStartColor, mProgressEndColor, Shader.TileMode.CLAMP
                    )
                    val matrix = Matrix()
                    matrix.setRotate(LINEAR_START_DEGREE, mCenterX, mCenterY)
                    shader.setLocalMatrix(matrix)
                }
                RADIAL -> {
                    shader = RadialGradient(
                        mCenterX, mCenterY, mRadius,
                        mProgressStartColor, mProgressEndColor, Shader.TileMode.CLAMP
                    )
                }
                SWEEP -> {
                    //arc = radian * radius
                    val radian = (mProgressStrokeWidth / Math.PI * 2.0f / mRadius).toFloat()

                    val rotateDegrees :Float=
                        (if (mCap == Paint.Cap.BUTT && mStyle == SOLID_LINE) 0f else (Math.toDegrees(radian.toDouble())).toFloat())

                    val value = mEndProgress / 100f
                    shader = SweepGradient(
                        mCenterX, mCenterY, intArrayOf(mProgressStartColor, mProgressEndColor),
                        floatArrayOf(0.0f, value)
                    )
                    val matrix = Matrix()
                    matrix.setRotate(rotateDegrees, mCenterX, mCenterY)
                    shader.setLocalMatrix(matrix)
                }
                else -> {
                }
            }

            if (mProgressStartColor != mProgressEndColor) {
                mProgressPaint.shader = shader
            }
        } else {
            mProgressPaint.shader = null
            //            mProgressPaint.setColor(mProgressStartColor);
        }
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        canvas.save()
        canvas.rotate(mStartDegree.toFloat(), mCenterX, mCenterY)
        drawProgress(canvas)
        canvas.restore()

        drawProgressText(canvas)
    }

    private fun drawProgressText(canvas: Canvas) {
        if (mProgressFormatter == null) {
            return
        }

        var proVal :Int = 0
        if (mProgressGap > 0) {
            proVal = (progress /mProgressGap)
        }
        val progressText = proVal.toString()
       // val progressText = mProgressFormatter!!.format(progress, max)

        if (TextUtils.isEmpty(progressText)) {
            return
        }

        mProgressTextPaint.textSize = mProgressTextSize
        mProgressTextPaint.color = mProgressTextColor

        mProgressTextPaint.getTextBounds(progressText.toString(), 0, progressText.length, mProgressTextRect)
        canvas.drawText(
            progressText,
            0,
            progressText.length,
            mCenterX,
            mCenterY + mProgressTextRect.height() / 2,
            mProgressTextPaint
        )
    }

    private fun drawProgress(canvas: Canvas) {

        when (mStyle) {
            SOLID -> drawSolidProgress(canvas)
            SOLID_LINE -> drawSolidLineProgress(canvas)
            LINE -> drawLineProgress(canvas)
            else -> drawLineProgress(canvas)
        }
    }

    /**
     * In the center of the drawing area as a reference point , rotate the canvas
     */
    private fun drawLineProgress(canvas: Canvas) {
        val unitDegrees = (2.0f * Math.PI / mLineCount).toFloat()
        val outerCircleRadius = mRadius
        val interCircleRadius = mRadius - mLineWidth

        val progressLineCount = (progress.toFloat() / max.toFloat() * mLineCount).toInt()

        for (i in 0 until mLineCount) {
            val rotateDegrees = i * -unitDegrees

            val startX = mCenterX + Math.cos(rotateDegrees.toDouble()).toFloat() * interCircleRadius
            val startY = mCenterY - Math.sin(rotateDegrees.toDouble()).toFloat() * interCircleRadius

            val stopX = mCenterX + Math.cos(rotateDegrees.toDouble()).toFloat() * outerCircleRadius
            val stopY = mCenterY - Math.sin(rotateDegrees.toDouble()).toFloat() * outerCircleRadius

            if (mDrawBackgroundOutsideProgress) {
                if (i >= progressLineCount) {
                    canvas.drawLine(startX, startY, stopX, stopY, mProgressBackgroundPaint)
                }
            } else {
                canvas.drawLine(startX, startY, stopX, stopY, mProgressBackgroundPaint)
            }

            if (i < progressLineCount) {

                val percententage = mLineCount.toFloat() / 100f
                val rCount = (percententage * mRotateCount).toInt()
                //Log.e(" Non Rotate ", "drawLineProgress: "+i+"   "+rCount);
                var remaining = rCount + mLineCount / 3
                remaining = remaining - mLineCount
                if (i < rCount || i > rCount + mLineCount / 3) {
                    //  Log.e(" Non Rotate ", "drawLineProgress: "+i+"   "+rCount);
                    canvas.drawLine(startX, startY, stopX, stopY, mProgressPaint)
                } else {
                    //Log.e(" Rotate ", "drawLineProgress: "+i+"   "+rCount);
                    canvas.drawLine(startX, startY, stopX, stopY, mProgressRotatePaint)
                }
            }
        }
    }


    fun updateRotation(count: Int) {
        mRotateCount = count
        invalidate()
    }

    /**
     * Just draw arc
     */
    private fun drawSolidProgress(canvas: Canvas) {
        if (mDrawBackgroundOutsideProgress) {
            val startAngle = MAX_DEGREE * progress / max
            val sweepAngle = MAX_DEGREE - startAngle
            canvas.drawArc(mProgressRectF, startAngle, sweepAngle, true, mProgressBackgroundPaint)
        } else {
            canvas.drawArc(mProgressRectF, 0.0f, MAX_DEGREE, true, mProgressBackgroundPaint)
        }
        canvas.drawArc(mProgressRectF, 0.0f, MAX_DEGREE * progress / max, true, mProgressPaint)
    }

    /**
     * Just draw arc
     */
    private fun drawSolidLineProgress(canvas: Canvas) {
        if (mDrawBackgroundOutsideProgress) {
            val startAngle = MAX_DEGREE * progress / max
            val sweepAngle = MAX_DEGREE - startAngle
            canvas.drawArc(mProgressRectF, startAngle, sweepAngle, false, mProgressBackgroundPaint)
        } else {
            canvas.drawArc(mProgressRectF, 0.0f, MAX_DEGREE, false, mProgressBackgroundPaint)
        }
        canvas.drawArc(mProgressRectF, 0.0f, MAX_DEGREE * progress / max, false, mProgressPaint)
    }

    /**
     * When the size of ColorPercentageMeter changed, need to re-adjust the drawing area
     */
    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        mCenterX = (w / 2).toFloat()
        mCenterY = (h / 2).toFloat()

        mRadius = Math.min(mCenterX, mCenterY)
        mProgressRectF.top = mCenterY - mRadius
        mProgressRectF.bottom = mCenterY + mRadius
        mProgressRectF.left = mCenterX - mRadius
        mProgressRectF.right = mCenterX + mRadius

        updateProgressShader()

        //Prevent the progress from clipping
        mProgressRectF.inset(mProgressStrokeWidth / 2, mProgressStrokeWidth / 2)
    }

    fun setProgressFormatter(progressFormatter: ColorPercentageMeter.ProgressFormatter) {
        this.mProgressFormatter = progressFormatter
        invalidate()
    }

    fun setProgressStrokeWidth(progressStrokeWidth: Float) {
        this.mProgressStrokeWidth = progressStrokeWidth
        mProgressRectF.inset(mProgressStrokeWidth / 2, mProgressStrokeWidth / 2)
        invalidate()
    }

    fun setProgressTextSize(progressTextSize: Float) {
        this.mProgressTextSize = progressTextSize
        invalidate()
    }

    fun setProgressStartColor(progressStartColor: Int) {
        this.mProgressStartColor = progressStartColor
        updateProgressShader()
        invalidate()
    }

    fun setProgressEndColor(progressEndColor: Int) {
        this.mProgressEndColor = progressEndColor
        updateProgressShader()
        invalidate()
    }

    fun setProgressTextColor(progressTextColor: Int) {
        this.mProgressTextColor = progressTextColor
        invalidate()
    }

    fun setProgressBackgroundColor(progressBackgroundColor: Int) {
        this.mProgressBackgroundColor = progressBackgroundColor
        mProgressBackgroundPaint.color = mProgressBackgroundColor
        invalidate()
    }

    fun setLineCount(lineCount: Int) {
        this.mLineCount = lineCount
        invalidate()
    }

    fun setLineWidth(lineWidth: Float) {
        this.mLineWidth = lineWidth
        invalidate()
    }

    fun setStyle(style: Int) {
        this.mStyle = style
        mProgressPaint.style = if (mStyle == SOLID) Paint.Style.FILL else Paint.Style.STROKE
        mProgressBackgroundPaint.style = if (mStyle == SOLID) Paint.Style.FILL else Paint.Style.STROKE
        invalidate()
    }

    fun setShader(shader: Int) {
        mShader = shader
        updateProgressShader()
        invalidate()
    }

    fun setCap(cap: Paint.Cap) {
        mCap = cap
        mProgressPaint.strokeCap = cap
        mProgressBackgroundPaint.strokeCap = cap
        invalidate()
    }

    interface ProgressFormatter {
        fun format(progress: Int, max: Int): CharSequence
    }

    private class DefaultProgressFormatter : ColorPercentageMeter.ProgressFormatter {

        override fun format(progress: Int, max: Int): CharSequence {
            return String.format(DEFAULT_PATTERN, (progress.toFloat() / max.toFloat() * 100).toInt())
        }

        companion object {
            private val DEFAULT_PATTERN = "%d%%"
        }
    }

    /*    private class SavedState : View.BaseSavedState {
            internal var progress: Int = 0

            internal constructor(superState: Parcelable) : super(superState) {}

            private constructor(`in`: Parcel) : super(`in`) {
                progress = `in`.readInt()
            }

            override fun writeToParcel(out: Parcel, flags: Int) {
                super.writeToParcel(out, flags)
                out.writeInt(progress)
            }

            companion object {

                val CREATOR: Parcelable.Creator<ColorPercentageMeter.SavedState> =
                    object : Parcelable.Creator<ColorPercentageMeter.SavedState> {
                        override fun createFromParcel(`in`: Parcel): ColorPercentageMeter.SavedState {
                            return ColorPercentageMeter.SavedState(`in`)
                        }

                        override fun newArray(size: Int): Array<ColorPercentageMeter.SavedState> {
                            return arrayOfNulls(size)
                        }
                    }
            }
        }

        public override fun onSaveInstanceState(): Parcelable? {
            // Force our ancestor class to save its state
            val superState = super.onSaveInstanceState()
            val ss = ColorPercentageMeter.SavedState(superState)

            ss.progress = progress

            return ss
        }

        public override fun onRestoreInstanceState(state: Parcelable) {
            val ss = state as ColorPercentageMeter.SavedState
            super.onRestoreInstanceState(ss.superState)

            progress = ss.progress
        }
    */
    companion object {
        private val DEFAULT_MAX = 100
        private val MAX_DEGREE = 360.0f
        private val LINEAR_START_DEGREE = 90.0f

        private val LINE = 0
        private val SOLID = 1
        private val SOLID_LINE = 2

        private val LINEAR = 0
        private val RADIAL = 1
        private val SWEEP = 2
        private val rotateLineCount = 8

        private val DEFAULT_START_DEGREE = -90

        private val DEFAULT_LINE_COUNT = 45

        private val DEFAULT_LINE_WIDTH = 8.0f
        private val DEFAULT_PROGRESS_TEXT_SIZE = 11.0f
        private val DEFAULT_PROGRESS_STROKE_WIDTH = 1.0f
        private val DEFAULT_PROGRESS_BACKGROUND_STROKE_WIDTH = 3.0f

        private val COLOR_FFF2A670 = "#fff2a670"
        private val COLOR_FFD3D3D5 = "#ffe3e3e5"
    }

}

