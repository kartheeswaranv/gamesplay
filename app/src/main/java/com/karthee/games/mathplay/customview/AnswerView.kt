package com.karthee.games.mathplay.customview

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.support.v4.content.ContextCompat
import android.text.Html
import android.util.AttributeSet
import com.karthee.games.mathplay.R
import com.karthee.games.mathplay.models.Answer
import com.karthee.games.uimodule.TextViewCustom

class AnswerView : ConstraintLayout {

  lateinit var mAnswerText:TextViewCustom
  lateinit var mAnswerStatus:TextViewCustom
  lateinit var mAnswer: Answer
  constructor(context: Context?) : super(context)
  constructor(
    context: Context?,
    attrs: AttributeSet?
  ) : super(context, attrs)

  constructor(
    context: Context?,
    attrs: AttributeSet?,
    defStyleAttr: Int
  ) : super(context, attrs, defStyleAttr)

  override fun onFinishInflate() {
    super.onFinishInflate()
    init()
  }

  fun init() {
    mAnswerStatus = findViewById(R.id.answer_status)
    mAnswerText = findViewById(R.id.answer)
  }

  public fun updateView(answer: Answer) {
    mAnswer = answer
    mAnswerText.text = answer.value.toString()
    mAnswerStatus.text=""
  }

  fun updateStatus() {
    if(!mAnswer.status) {
      mAnswerStatus.text = "X"
      mAnswerStatus.setTextColor(ContextCompat.getColor(context,R.color.validation_failure))
    } else {
      mAnswerStatus.text = Html.fromHtml(context.getString(R.string.tick_mark))
      mAnswerStatus.setTextColor(ContextCompat.getColor(context,R.color.validation_success))
    }
  }



}