package com.karthee.games.mathplay.customview

import android.content.Context
import android.os.CountDownTimer
import android.support.constraint.ConstraintLayout
import android.util.AttributeSet
import com.karthee.games.mathplay.R
import com.karthee.games.mathplay.models.Answer
import com.karthee.games.mathplay.models.Question
import com.karthee.games.uimodule.TextViewCustom

class QuestionView : ConstraintLayout {

  lateinit var mAnswer1: AnswerView
  lateinit var mAnswer2: AnswerView
  lateinit var mAnswer3: AnswerView
  lateinit var mAnswer4: AnswerView
  lateinit var mQuestionTxt: TextViewCustom

  lateinit var mQuestion: Question

  lateinit var mTimer: CountDownTimer

  interface QuestionCallback {
    fun onFinishQuestion(success: Boolean)
    fun onFinishTimer()
  }

  lateinit var mCallback: QuestionCallback

  constructor(context: Context?) : super(context)
  constructor(
    context: Context?,
    attrs: AttributeSet?
  ) : super(context, attrs)

  constructor(
    context: Context?,
    attrs: AttributeSet?,
    defStyleAttr: Int
  ) : super(context, attrs, defStyleAttr)

  override fun onFinishInflate() {
    super.onFinishInflate()
    init()
  }

  fun setQuestionCallback(callback: QuestionCallback) {
    mCallback = callback
  }

  fun init() {
    mQuestionTxt = findViewById(R.id.question_view)
    mAnswer1 = findViewById(R.id.answer1)
    mAnswer2 = findViewById(R.id.answer2)
    mAnswer3 = findViewById(R.id.answer3)
    mAnswer4 = findViewById(R.id.answer4)

    mAnswer1.setOnClickListener { updateStatusForTime(mQuestion.list[0]) }

    mAnswer2.setOnClickListener { updateStatusForTime(mQuestion.list[1]) }

    mAnswer3.setOnClickListener { updateStatusForTime(mQuestion.list[2]) }

    mAnswer4.setOnClickListener { updateStatusForTime(mQuestion.list[3]) }

  }

  public fun updateView(question: Question) {
    mQuestion = question
    mQuestionTxt.text = mQuestion.question

    updateAnswers()
  }

  fun updateAnswers() {

    mAnswer1.updateView(mQuestion.list[0])
    mAnswer2.updateView(mQuestion.list[1])
    mAnswer3.updateView(mQuestion.list[2])
    mAnswer4.updateView(mQuestion.list[3])
  }

  fun updateStatus() {
    mAnswer1.updateStatus()
    mAnswer2.updateStatus()
    mAnswer3.updateStatus()
    mAnswer4.updateStatus()
  }

  fun updateStatusForTime(answer: Answer) {

    updateStatus()
    if (mCallback != null)
      mCallback.onFinishTimer()
    mTimer = object : CountDownTimer(1500, 1510) {
      override fun onTick(millisUntilFinished: Long) {

      }

      override fun onFinish() {
        if (mCallback != null)
          mCallback.onFinishQuestion(isAnswerCorrect(answer))
      }
    }
    mTimer.start()
  }

  fun isAnswerCorrect(answer: Answer): Boolean {
    return answer.status
  }
}