package com.karthee.games.mathplay.customview

import android.animation.ValueAnimator
import android.content.Context
import android.support.constraint.ConstraintLayout
import android.util.AttributeSet
import com.karthee.games.mathplay.R
import com.karthee.games.uimodule.TextViewCustom
import com.karthee.games.uimodule.progressviews.ColorPercentageMeter

class HeaderView :ConstraintLayout {

    var  mAnimator :ValueAnimator? = null
    lateinit var mScoreText: TextViewCustom
    lateinit var mCountText: TextViewCustom
    lateinit var mTimerMeter : ColorPercentageMeter

    interface TimerCallback {
        fun onFinishTime()
    }

    lateinit  var mAnimationCallback:TimerCallback
    constructor(context: Context?) : super(context)
    constructor(
        context: Context?,
        attrs: AttributeSet?
    ) : super(context, attrs)

    constructor(
        context: Context?,
        attrs: AttributeSet?,
        defStyleAttr: Int
    ) : super(context, attrs, defStyleAttr)

    override fun onFinishInflate() {
        super.onFinishInflate()
        init()
    }

    fun init() {
        mScoreText = findViewById(R.id.score)
        mCountText = findViewById(R.id.count)
        mTimerMeter = findViewById(R.id.timer)
    }


    fun setAnimationCallback(callback:TimerCallback) {
        mAnimationCallback = callback
    }

    fun updateHeaderView(scoreTxt:String,countTxt:String) {
        mScoreText.text = context.getString(R.string.score_txt,scoreTxt)
        mCountText.text = context.getString(R.string.count_txt,countTxt)
        pauseTimer()
        updateTimer()
    }

    fun pauseTimer() {
        if(mAnimator != null)mAnimator?.cancel()
   }

    internal fun updateTimer() {
        mTimerMeter.mProgressGap = 10
        mTimerMeter.max =100
        mAnimator = ValueAnimator.ofInt(0, 100)
        mAnimator?.addUpdateListener {
            var progress :Int= it?.animatedValue as Int
            mTimerMeter.progress= 100-progress
            if(progress == 100 && mAnimationCallback != null) {
                mAnimationCallback.onFinishTime()
            }
        }
        mAnimator?.duration =10000
        mAnimator?.start()
    }


}