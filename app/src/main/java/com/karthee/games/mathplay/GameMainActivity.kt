package com.karthee.games.mathplay

import android.os.Bundle
import android.os.CountDownTimer
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.karthee.games.mathplay.customview.HeaderView
import com.karthee.games.mathplay.customview.HeaderView.TimerCallback
import com.karthee.games.mathplay.customview.QuestionView
import com.karthee.games.mathplay.customview.QuestionView.QuestionCallback
import com.karthee.games.mathplay.helpers.QuestionHelper
import com.karthee.games.mathplay.models.Question
import com.karthee.games.uimodule.StatusBarUtil
import com.karthee.games.uimodule.logger.LogUtils

class GameMainActivity : AppCompatActivity() {

  private val TAG :String= LogUtils.makeLogTag(GameMainActivity::class.java.simpleName)
  private lateinit var mTimer: CountDownTimer

  lateinit var mQuestionView: QuestionView
  lateinit var mHelper: QuestionHelper
  lateinit var mHeaderView: HeaderView

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_game_main)
    StatusBarUtil.setTranslucent(this)
    init()
    mHelper = QuestionHelper()
    updateQuestion()
  }

  fun init() {
    mQuestionView = findViewById(R.id.questions)
    mHeaderView = findViewById(R.id.header)
    mQuestionView.setQuestionCallback(object:QuestionCallback{
      override fun onFinishQuestion(success: Boolean) {
          mHelper.setCorrectAnswer(success)
        updateQuestion()
      }

      override fun onFinishTimer() {
        mHeaderView.pauseTimer()
      }

    })

    mHeaderView.setAnimationCallback(object:TimerCallback {

      override fun onFinishTime() {
        LogUtils.LOGD(TAG,"onFinishTime : ")
         if(mHelper.isLastQuestion()) {

         } else {

           updateQuestion()
         }
      }
    })
  }


  fun updateQuestion() {
    if(!mHelper.isLastQuestion()) {
      var q: Question = mHelper.getQuestion()

      var count:String= mHelper.getQuestionCount().toString()+"/"+mHelper.getMaxQuestionCount()
      var score:String = mHelper.getAnswerCount().toString()
      mHeaderView.updateHeaderView(score,count)
      mQuestionView.updateView(q)
    } else {
      Toast.makeText(this@GameMainActivity,"Finish the Game ",Toast.LENGTH_LONG).show()
    }
  }

}
