package com.karthee.games.mathplay.helpers

import android.util.Log
import com.karthee.games.mathplay.models.Answer
import com.karthee.games.mathplay.models.Question
import com.karthee.games.uimodule.logger.LogUtils
import java.util.*

class QuestionHelper {
  lateinit var mQuestionValues:IntArray
  lateinit var mCurrentQuestion :Question
  private  var mQuestionCount:Int =0
  private var mCorrectAnsCount = 0
  private var mMaxQuestion = 10
  constructor() {
    mQuestionValues = IntArray(2)
  }

  constructor(value:Int) {
    mQuestionValues = IntArray(value)
  }

  fun isLastQuestion():Boolean {
    if(mQuestionCount == mMaxQuestion)
      return true

    return false
  }


  fun getQuestionCount() :Int {
    return mQuestionCount;
  }

  fun getAnswerCount() : Int{
    return mCorrectAnsCount
  }

  fun getMaxQuestionCount():Int {
    return mMaxQuestion
  }

  fun getQuestion() :Question {
    var rand  = Random()
    var answer= 0
    var quesStr = ""
    for( index in 0 until mQuestionValues.size){
      mQuestionValues[index] = rand.nextInt(15)
      answer += mQuestionValues[index]
      quesStr += " "+mQuestionValues[index]
      if(index < mQuestionValues.size-1)
        quesStr +=" +"
    }

    quesStr +=" = ?"


    var random : Random = Random()

    var eachPart:Int = answer/4

    if(answer < 4 ) {
        eachPart = 4
    }

    var answerValue1 = random.nextInt(eachPart)+eachPart
    var answerValue2 = random.nextInt(eachPart)+3*eachPart

    var answerValue3:Int =random.nextInt(eachPart)+5*eachPart
    LogUtils.LOGD("QuestionHelper","Answer : "+answer)
    LogUtils.LOGD("QuestionHelper"," "+eachPart+"   "+answerValue1+"  "+answerValue2+"   "+answerValue3)

    var list = generateAnswersValue(answer)

    mCurrentQuestion =Question(quesStr,list)

    mQuestionCount++
    return mCurrentQuestion
  }

  fun setCorrectAnswer(yes:Boolean) {
    if(yes) {
      mCorrectAnsCount++
    }
  }

  fun generateAnswersValue(answer:Int):List<Answer> {


    var randomN = listOf((answer-7)..(answer-1))
    var randomN1 = listOf((answer+1)..(answer+7))

    if(answer < 7) {
      randomN = listOf((answer+7)..(answer+14))
    }
    var list = mutableListOf<Answer>()

    randomN = randomN.shuffled()
    randomN1 = randomN1.shuffled()



/*    if(answer != randomN[0].first) {
      list.add(Answer(randomN[0].first, false))
    } else {
      list.add(Answer(answer,true))
    }
    if(answer != randomN[0].last) {
      list.add(Answer(randomN[0].last, false))
    } else {
      list.add(Answer(answer,true))
    }

    if(answer != randomN[randomN.size-1].first) {
      list.add(Answer(randomN[randomN.size - 1].first, false))
    } else {
      list.add(Answer(answer,true))
    }

    if(answer != randomN[randomN.size-1].last) {
      list.add(Answer(randomN[randomN.size - 1].last, false))
    } else {
      list.add(Answer(answer,true))
    }

    if(answer != randomN[randomN.size-1].first) {
      list.add(Answer(randomN[randomN.size - 1].first, false))
    } else {
      list.add(Answer(answer,true))
    }
    list.shuffle()*/

    list.add(Answer(randomN[0].first,false))
    list.add(Answer(randomN[0].last,false))
    list.add(Answer(randomN1[0].first,false))
    list.add(Answer(randomN1[0].last,false))

    var rand :Random = Random()
    var index = rand.nextInt(list.size-1)
    list.set(index,Answer(answer,true))

    list.shuffle()

    return list
  }
}