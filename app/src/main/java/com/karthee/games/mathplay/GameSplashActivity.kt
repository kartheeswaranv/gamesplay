package com.karthee.games.mathplay

import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.karthee.games.uimodule.StatusBarUtil

class GameSplashActivity : AppCompatActivity() {


    private lateinit var mTimer: CountDownTimer

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game_splash)
        StatusBarUtil.setTranslucent(this)
        runSplashScreenTime()
    }

    fun runSplashScreenTime() {
        mTimer = object : CountDownTimer(3000, 3010) {

            override fun onTick(millisUntilFinished: Long) {

            }

            override fun onFinish() {
                Toast.makeText(this@GameSplashActivity, "OnFinish  ", Toast.LENGTH_LONG).show()
                launchMainPage()

            }

        }
        mTimer.start()
    }


    override fun onDestroy() {
         mTimer.cancel()
        super.onDestroy()
    }

    fun launchMainPage() {
        var intent = Intent(this,GameMainActivity::class.java)
        startActivity(intent)
        finish()
    }
}
